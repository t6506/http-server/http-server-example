package org.example.exception;

public class ItemNotFoundException extends RuntimeException {
    public ItemNotFoundException(long id) {
        super("item by id " + id + " not found");
    }
}
