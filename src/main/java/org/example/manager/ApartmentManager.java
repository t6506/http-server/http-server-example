package org.example.manager;

import lombok.extern.slf4j.Slf4j;
import org.example.domain.Apartment;
import org.example.dto.ApartmentRQ;
import org.example.dto.ApartmentRS;
import org.example.exception.ItemNotFoundException;
import org.example.exception.UserNotAuthorizedException;
import org.example.framework.auth.AnonymousPrincipal;

import java.security.Principal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class ApartmentManager {
    private long nextId = 1;
    private final List<Apartment> items = new ArrayList<>();

    public ApartmentRS create(final ApartmentRQ createRQ, final Principal principal) throws UserNotAuthorizedException {
        if (principal.getName().equals(AnonymousPrincipal.ANONYMOUS)) {
            throw new UserNotAuthorizedException("user " + principal.getName() + " not authorized for operation");
        }
        final Apartment item = new Apartment(
                nextId,
                principal.getName(),
                Instant.now().getEpochSecond(),
                createRQ.getNumberOfRooms(),
                createRQ.getPrice(),
                createRQ.getArea(),
                createRQ.isBalcony(),
                createRQ.isLoggia(),
                createRQ.getFloor(),
                createRQ.getFloorsInHouse()
        );
        log.debug("create item, nextId: {}, apartment: {}", nextId, item);
        items.add(item);
        nextId += 1;
        return new ApartmentRS(item);
    }

    public Apartment getById(final long id) throws ItemNotFoundException{
        for (final Apartment item : items) {
            if (item.getId() == id) {
                log.debug("return item: {}", item);
                return item;
            }
        }
        log.debug("item by id {} not found", id);
        throw new ItemNotFoundException(id);
    }

    public List<ApartmentRS> getAll() {
        return items.stream()
                .map(ApartmentRS::new)
                .collect(Collectors.toList());
    }

    public void removeById(final long id, final Principal principal) throws
            ItemNotFoundException, UserNotAuthorizedException {
        if (!getById(id).getOwner().equals(principal.getName())) {
            throw new UserNotAuthorizedException("user " + principal.getName() + " not authorized for operation");
        }
        final boolean removed = items.removeIf(o -> o.getId() == id);
        if (!removed) {
            log.debug("remove item by Id {} fault", id);
            throw new ItemNotFoundException(id);
        }
        log.debug("remove item by id {} success", id);
    }

    public ApartmentRS update(final ApartmentRQ updateRQ, final long id, final Principal principal)
            throws ItemNotFoundException, UserNotAuthorizedException {
        final Apartment item = getById(id);
        removeById(id, principal);
        final Apartment updatedItem = new Apartment(
                id,
                principal.getName(),
                item.getCreated(),
                updateRQ.getNumberOfRooms(),
                updateRQ.getPrice(),
                updateRQ.getArea(),
                updateRQ.isBalcony(),
                updateRQ.isLoggia(),
                updateRQ.getFloor(),
                updateRQ.getFloorsInHouse()
        );
        log.debug("update item id {}", id);
        items.add(updatedItem);
        return new ApartmentRS(updatedItem);
    }

    public int getCount() {
        return items.size();
    }

    private int getIndexById(final long id) {
        for (int i = 0; i < items.size(); i++) {
            final Apartment apartment = items.get(i);
            if (apartment.getId() == id) {
                log.debug("return index: {} by id: {}", i, id);
                return i;
            }
        }
        log.debug("index by id: {} not found", id);
        return -1;
    }
}
